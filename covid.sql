-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-02-2021 a las 22:06:59
-- Versión del servidor: 10.4.13-MariaDB
-- Versión de PHP: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `covid`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `casos`
--

CREATE TABLE `casos` (
  `id` int(2) NOT NULL,
  `ubicacion` varchar(20) DEFAULT NULL,
  `casos` varchar(9) DEFAULT NULL,
  `nuevos` varchar(5) DEFAULT NULL,
  `muertes` varchar(7) DEFAULT NULL,
  `poblacion` varchar(11) DEFAULT NULL,
  `mujeres` varchar(10) DEFAULT NULL,
  `hombres` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `casos`
--

INSERT INTO `casos` (`id`, `ubicacion`, `casos`, `nuevos`, `muertes`, `poblacion`, `mujeres`, `hombres`) VALUES
(1, 'Mexico', '2,038,276', '7,785', '179,797', '103,498,524', '53,238,151', '50,260,373'),
(2, 'Ciudad de Mexico', '538,974', '2,916', '26,167', '8,737,172', '4,611,823', '4,125,349'),
(3, 'Estado de Mexico', '211,447', '913', '27,724', '14,174,039', '7,305,419', '6,868,620'),
(4, 'Guanajuato', '116,774', '445', '8,737', '4,894,608', '2,556,700', '2,337,908'),
(5, 'Nuevo Leon', '113,629', '242', '7,913', '4,199,361', '2,091,374', '2,107,987'),
(6, 'Jalisco', '76,163', '307', '10,125', '6,754,506', '3,434,687', '3,319,819'),
(7, 'Puebla', '68,523', '275', '8,396', '5,386,250', '2,819,146', '2,567,104'),
(8, 'Sonora', '66,876', '95', '5,873', '2,395,297', '1,209,796', '1,185,501'),
(9, 'Coahuila de Zaragoza', '63,980', '216', '5,593', '2,501,413', '1,237,003', '1,264,410'),
(10, 'Tabasco', '56,359', '132', '3,671', '1,991,059', '1,000,622', '990,437'),
(11, 'San Luis Potos?', '55,982', '235', '4,378', '2,413,811', '1,250,012', '1,163,799'),
(12, 'Queretaro', '54,609', '356', '3,266', '1,598,179', '809,658', '788,521'),
(13, 'Veracruz', '54,255', '147', '7,945', '7,117,300', '3,703,836', '3,413,464'),
(14, 'Tamaulipas', '51,001', '104', '4,287', '3,024,417', '1,524,374', '1,500,043'),
(15, 'Baja California', '44,231', '56', '7,280', '2,856,361', '1,433,655', '1,422,706'),
(16, 'Michoacan', '42,574', '73', '4,329', '3,965,878', '2,074,305', '1,891,573'),
(17, 'Chihuahua', '42,438', '80', '5,108', '3,241,513', '1,645,340', '1,596,173'),
(18, 'Oaxaca', '39,527', '231', '2,799', '3,508,986', '1,815,607', '1,693,379'),
(19, 'Guerrero', '34,658', '110', '3,513', '3,115,237', '1,641,311', '1,473,926'),
(20, 'Sinaloa', '33,433', '145', '5,095', '2,611,306', '1,344,295', '1,267,011'),
(21, 'Yucatan', '32,066', '108', '3,000', '1,820,537', '920,493', '900,044'),
(22, 'Durango', '30,515', '78', '2,043', '1,509,025', '776,839', '732,186'),
(23, 'Zacatecas', '27,005', '76', '2,378', '1,368,391', '702276', '666,115'),
(24, 'Baja California Sur', '25,805', '78', '1,154', '512,030', '244,539', '267,491'),
(25, 'Morelos', '25,185', '82', '2,214', '1,616,209', '844,987', '771,222'),
(26, 'Aguascalientes', '22,616', '60', '1,950', '1,066,233', '548,418', '517,815'),
(27, 'Quintana Roo', '19,872', '37', '2,365', '1,135,436', '541,790', '593,646'),
(28, 'Tlaxcala', '16,777', '59', '2,091', '1,068,467', '547,848', '520,619'),
(29, 'Nayarit', '10,662', '34', '1,581', '952,230', '481,661', '470,569'),
(30, 'Colima', '10,212', '0', '1,036', '568,642', '299,113', '269,529'),
(31, 'Chiapas', '9,886', '30', '1,411', '4,293,414', '2,233,257', '2,060,157'),
(32, 'Campeche', '8,372', '16', '1,049', '755,703', '385,299', '370,404'),
(33, 'Estado de Hidalgo', '344', '0', '9', '2,345,514', '1,202,668', '1,142,846');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `semanas`
--

CREATE TABLE `semanas` (
  `id` int(2) NOT NULL,
  `mes` varchar(8) DEFAULT NULL,
  `semana` int(1) DEFAULT NULL,
  `contagios` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `semanas`
--

INSERT INTO `semanas` (`id`, `mes`, `semana`, `contagios`) VALUES
(1, 'Enero', 1, 6474),
(2, 'Enero', 2, 7594),
(3, 'Enero', 3, 8074),
(4, 'Enero', 4, 8521),
(5, 'febrero ', 1, 5448),
(6, 'febrero', 2, 3868),
(7, 'febrero', 3, 3098),
(8, 'febrero', 4, 3104);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `casos`
--
ALTER TABLE `casos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `semanas`
--
ALTER TABLE `semanas`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
