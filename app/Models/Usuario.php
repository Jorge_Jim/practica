<?php
namespace UPT;

class Usuario
{
    public $valor;
    public $ubicacion;
    public $campo;

    public function __construct()
    {
        parent::__construct();
    }
    static function ubicaciones(){
        $cone = new Conexion();
        $ubi = mysqli_prepare($cone->con,"SELECT ubicacion FROM casos");
        $ubi->execute();
        $resultado = $ubi->get_result();
        while ($fila = mysqli_fetch_assoc($resultado)){
            echo '<option>'.$fila['ubicacion'].'</option>';
        }
    }
    static function covid(){
        $cone = new Conexion();
        $covid = mysqli_prepare($cone->con,"SELECT * FROM casos");
        $covid->execute();
        $resultado = $covid->get_result();

        while ($fila = mysqli_fetch_assoc($resultado)){
            echo '
                    <tr>
                        <td>'.$fila['ubicacion'].'</td>
                        <td>'.$fila['casos'].'</td>
                        <td>'.$fila['nuevos'].'</td>
                        <td>'.$fila['muertes'].'</td>
                        <td>'.$fila['poblacion'].'</td>
                        <td>'.$fila['mujeres'].'</td>
                        <td>'.$fila['hombres'].'</td>
                    <tr>';
        }
    }
    static function semanas(){
        $cone = new Conexion();
        $covid = mysqli_prepare($cone->con,"SELECT * FROM semanas");
        $covid->execute();
        $resultado = $covid->get_result();

        while ($fila = mysqli_fetch_assoc($resultado)){
            echo '
                    <tr>
                        <td>'.$fila['mes'].'</td>
                        <td>'.$fila['semana'].'</td>
                        <td>'.$fila['contagios'].'</td>
                    <tr>';
        }
    }

    function actualizar(){
        $cone = new Conexion();
        $actualizo = mysqli_prepare($cone->con,"UPDATE casos SET ubicacion=? AND ?=?");
        $actualizo->bind_param("sss",$this->ubicacion,$this->campo,$this->valor);
        $actualizo->execute();
    }
    static function eliminarUbi($ubicacion){
        $cone = new Conexion();
        $eliUbi = mysqli_prepare($cone->con,"DELETE FROM casos WHERE ubicacion = ?");
        $eliUbi->bind_param("s",$ubicacion);
        $eliUbi->execute();
    }
    static function eliminarMes($mes,$semana){
        $cone = new Conexion();
        $eliUbi = mysqli_prepare($cone->con,"DELETE FROM semanas WHERE mes = ? AND semana = ?");
        $eliUbi->bind_param("ss",$mes,$semana);
        $eliUbi->execute();
    }
}
