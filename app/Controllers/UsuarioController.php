<?php
require 'app/Models/Conexion.php';
require 'app/Models/Usuario.php';
use UPT\Usuario;
use UPT\Conexion;
class UsuarioController
{
    function inicio(){
        require "app/Views/inicio.php";
    }
    function actualizar(){
        require "app/Views/actualizar.php";
    }
    function eliminar(){
        require "app/Views/eliminar.php";
    }
    function agregar(){
        require "app/Views/Agregar.php";
    }

    function borrarUbicacion(){
        Usuario::eliminarUbi($_POST['ubica']);
        require "app/Views/eliminar.php";
    }
    function borrarMes(){
        Usuario::eliminarMes($_POST['Mes'],$_POST['semana']);
        require "app/Views/eliminar.php";
    }
    function actualizoCasos(){

    }
}