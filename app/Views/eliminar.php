<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Covid</title>
    <style>
        ::-webkit-scrollbar {
            display: none;
        }
        body{
            margin: 0;
            background-image: url("https://www.paho.org/sites/default/files/styles/flexslider_full/public/2020-03/blue-covid-banner.jpg?h=96546727&itok=cZemcbKa");
            background-size: cover;
            background-position: right bottom;
            color: white;
        }

        .header {
            background-color: #4CAF50;
            padding: 20px;
            text-align: center;
        }

        .topnav {
            overflow: hidden;
            background-color: #333;
        }

        .topnav a {
            float: left;
            display: block;
            color: #f2f2f2;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
        }

        .topnav a:hover {
            background-color: #ddd;
            color: black;
        }

        table, td, th {
            border: 1px solid black;
        }

        table {
            border-collapse: collapse;
            width: 70%;
            float: ;
        }

        th, td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even){background-color: #f2f2f2}

        th {
            background-color: #4CAF50;
            color: white;
        }
    </style>
</head>
<body>
<div class="header">
    <h1>Covid 19</h1>
    <p>Contagios en México</p>
</div>

<div class="topnav">
    <a href="index.php?controller=Usuario&action=inicio">Inicio</a>
    <a href="index.php?controller=Usuario&action=actualizar">Actualizar</a>
    <a href="index.php?controller=Usuario&action=eliminar" style="color: #4CAF50">Eliminar</a>
    <a href="index.php?controller=Usuario&action=agregar" >Agregar</a>
</div>
<form action="index.php?controller=Usuario&action=borrarUbicacion" method="post"><br>
    <select name="ubica">
        <?php
            \UPT\Usuario::ubicaciones();
        ?>
    </select>
    <input type="submit" value="Eliminar"><br><br>
</form>
<table>
    <tr>
        <th>Ubicacion</th>
        <th>Casos</th>
        <th>Casos nuevos</th>
        <th>Muertes</th>
        <th>Poblacion</th>
        <th>Mujeres</th>
        <th>Hombres</th>
    <tr>
        <?php
        \UPT\Usuario::covid();
        ?>
</table>
<form action="index.php?controller=Usuario&action=borrarMes" method="post">
    Mes: <input type="text" name="Mes"><br>
    Semana: <input type="number" name="semana"><br>
    <input type="submit" value="Eliminar">
</form>
<table>
    <tr>
        <th>Mes</th>
        <th>Semana</th>
        <th>Casos nuevos</th>
    <tr>
        <?php
        \UPT\Usuario::semanas();
        ?>
</table>
<br>
</body>
</html>